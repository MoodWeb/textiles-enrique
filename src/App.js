import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AppPrincipal from './Componentes/AppPrincipal';
import Navbar from './Componentes/Navbar';
import About from './Componentes/About';
import Products from './Componentes/Products';
import Contact from './Componentes/Contact';
import Footer from './Componentes/Footer';

function App() {

  return (
                  <Router>
                      <Navbar/>
                        <Switch>
                          <Route exact path="/" component={ AppPrincipal } />
                          <Route exact path="/about" component={ About } />
                          <Route exact path="/products" component={ Products } />
                          <Route exact path="/contact" component={ Contact } />
                        </Switch>      
                      <Footer/>
                  </Router>
  );
}

export default App;