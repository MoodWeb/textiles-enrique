import React, { Component} from 'react';
import styled from "styled-components";

const AboutContainer = styled.div`
    width:100%;
    height: auto;
    display:flex;
    @media (max-width:375px){
        display:block;
    }
    @media(max-width:320px){
        display:block;
    }
    @media (max-width:414px){
        display:block;
    }
`;
const AboutLeft = styled.div`
    width:50%;
    background-color:#C8102E;
    @media (max-width:375px){
        width:100%;
    }
    @media (max-width:320px){
        width:100%;
    }
    @media (max-width:414px){
        width:100%
    }
`;
const AboutRight = styled.div`
    width:50%;
    background-color:#ffffff;
    @media (max-width:375px){
        width:100%;
    }
    @media (max-width:320px){
        width:100%;
    }
    @media (max-width:414px){
        width:100%;
    }
`;
const AboutTitle = styled.h1`
    position:relative;
    text-shadow: 0 0 4px #ffffff;
    color:#C8102E;
    font-size: 11vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-align:center;
    text-align:center;
    animation-duration: 3s;
    animation-name: title;
    animation-direction: alternate-reverse;
    animation-iteration-count: 2;
    animation-timing-function: ease-out;
    margin:0;
    margin-top:20%;
    @media (max-width:375px){
        margin:0;
    }
    @media (max-width:320px){
        margin:0;
    }
    @media (max-width:414px){
        margin:0;
    }
    @keyframes title {
        from {
            right: 0;
        } 
        to {
            right: 80px;
        }
    
`;
const Who = styled.h2`
    font-family:'Ubuntu', sans-serif;
    font-size:2.5vw;
    font-weight:700;
    text-align:center;
    color:#C8102E;
    @media (max-width:375px){
        margin:0;
    }
    @media (max-width:320px){
        margin:0;
    }
    @media (max-width:414px){
        margin:0;
    }
`;  
const SubAbout = styled.h3`
    position:absolute;
    font-size: 7vw;
    font-family:"Ubuntu", sans-serif;;
    font-weight:700;
    color:#330CE9;
    margin:0;
    margin-left:17%;
    z-index:100000;
    margin-top:-120px;
    @media (max-width:375px){
        margin-left:42%;
        margin-top:-10%;
    }
    @media (max-width:320px){
        margin-left:42%;
        margin-top:-10%;
    }
    @media (max-width:414px){
        margin-left:42%;
        margin-top:-10%;
    }
`;

const Text = styled.p`
    width:90%;
    font-family: 'Ubuntu', sans-serif;
    font-size:1.3vw;
    font-weight:400;
    text-align: right;
    color:#424949;
    line-height:2.9vw;
    margin:5% 5%;
    @media (max-width:375px){
        margin:0 5% 0 5%;
        text-align:center;
    }
    @media (max-width:320px){
        margin:0 5% 0 5%;
        text-align:center;
    }
    @media (max-width:414px){
        margin:0 5% 0 5%;
        text-align:center;
    }

`;
class About extends Component {
    render(){
        return(
            <AboutContainer>
                <AboutLeft>
                    <AboutTitle>Nosotros</AboutTitle>
                </AboutLeft>
                <AboutRight>
                    <Text>
                        <Who>¿Quienes somos?</Who>
                        Fundada en 2002 y con oficinas generales en el Centro Histórico de La Ciudad de México, 
				Textiles Enrique ofrece una amplia gama de telas y textiles para la industria textil.
				Somos proveedores de Telas y Textiles para las mas diversas aplicaciones en la dinámica industria textil.

				Toda la gama de nuestros productos ofrece calidad consistente y al mejor precio de todo México, 
				desde que realizan su pedido hasta el momento de la entrega. 
				En Textiles Enrique estamos orgullosos de ser uno de los proveedores de telas y textiles mas confiables de México. 
				Siempre vamos un paso mas allá y buscamos exceder expectativas porque nuestros clientes son nuestra prioridad y su éxito es lo más importante.
                    </Text>
                </AboutRight>
            </AboutContainer>
        )
    }
}






export default About;