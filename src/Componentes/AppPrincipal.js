import React from 'react';
import About from './About';
import Products from './Products';
import Contact from './Contact';
import Initview from './Initview';

function AppPrincipal() {
  return (
    <React.Fragment>
      <Initview/>
      <About/>
      <Products/>
      <Contact/>
    </React.Fragment>
  );
}

export default AppPrincipal;