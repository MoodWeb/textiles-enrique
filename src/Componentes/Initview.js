import React, {Component} from 'react';
import styled from 'styled-components';
import { ReactPlayer } from 'react-player';

const ViewContainer = styled.div`
    width:100%;
    margin:0;
    padding:0;
`;

class Initview extends Component{
    render(){
        return(
            <ViewContainer>
                <ReactPlayer url='https://www.youtube.com/watch?v=3mIkWhWLls4&feature=youtu.be' playing/>
            </ViewContainer>
        )
    }
}

export default Initview;