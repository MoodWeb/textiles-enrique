import React, { Component } from "react";
import styled from "styled-components";

const ContactContainer = styled.div`
  width: 100%;
  background-color: #ffffff;
`;
const ContactDiv = styled.div`
  position: relative;
  width: 100%;
`;
const Formulario = styled.form`
  width: 95%;
  text-align: center;
  line-height: 10vh;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
  margin-top: 10%;
  margin-bottom: 10%;
  @media (max-width: 375px) {
    width: 100%;
  }
`;
const ContactLeft = styled.div`
  width: 100%;
  background-color: #c8102e;
  @media (max-width: 375px) {
    width: 100%;
  }
  @media (max-width: 414px) {
    width: 100%;
  }
`;
const TitleContact = styled.h1`
    font-size: 11vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-shadow: 0 0 4px #ffffff;
    text-align:center;
    color:#C8102E;
    position:relative;
    margin:0;
    padding:5% 0 0 0;
    animation-duration: 3s;
    animation-name: contacto;
    animation-direction: alternate-reverse;
    animation-iteration-count: 2;
    animation-timing-function: ease-out;
    @keyframes contacto {
        from {
            right: 0;
        } 
        to {
            right: 50px;
        }
`;
const TitleCampo = styled.label`
  font-family: "Ubuntu", sans-serif;
  font-size: 20px;
  font-weight: 400;
  color: #c8102e;
  margin-left: 5px;
`;
const Campo = styled.input`
  position: relative;
  width: 33%;
  height: 30px;
  margin-left: 5px;
  border: none;
  border-bottom: 2px solid #7b7d7d;
  color: #000;
  font-size: 12px;
  box-sizing: border-box;
  outline: 0;
  @media (max-width: 414px) {
    width: 100%;
  }
  &::placeholder {
    color: #c8102e;
    font-size: 15px;
    font-family: "Ubuntu", sans-serif;
    font-weight: 400;
    opacity: 0.5;
  }
  &:focus {
    border-bottom: 2px solid #c8102e;
  }
  @media (max-width: 375px) {
  }
`;
const BtnSend = styled.button`
  border-radius: 25px;
  cursor: pointer;
  width: 10%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: auto;
  margin-top: -7%;
  margin-bottom: 5%;
  height: 35px;
  background-color: #c8102e;
  color: #000;
  border-style: none;
  font-family: "Ubuntu", sans-serif;
  font-weight: 400;
  font-size: 20px;
  @media (max-width: 414px) {
    width: 100%;
  }
  &:hover {
      svg{
        fill:#ffffff;
      }
    color: #ffffff;
  }
`;
const Send = styled.svg`
  width: 2vw;
  margin: 0 3%;
  fill:#000;
`;
class Contact extends Component {
  nombreRef = React.createRef();
  apellidosRef = React.createRef();
  asuntoRef = React.createRef();
  telefonoRef = React.createRef();
  emailRef = React.createRef();

  enviarDatos = e => {
    e.preventDefault();
    const nombre = this.nombreRef.current.value;
    const apellidos = this.apellidosRef.current.value;
    const asunto = this.asuntoRef.current.value;
    const telefono = this.telefonoRef.current.value;
    const email = this.emailRef.current.value;

    alert(
      `El cliente ${nombre} ${apellidos} quiere que lo contacten para ${asunto}. \n Su telélefono es el ${telefono} y su correo electrónico es ${email}`
    );
  };

  render() {
    return (
      <ContactContainer>
        <ContactLeft>
          <TitleContact>CONTACTO</TitleContact>
        </ContactLeft>
        <ContactDiv>
          <Formulario>
            <Campo
              type="text"
              ref={this.nombreRef}
              maxLength="25"
              required
              placeholder="Nombre(s)"
            />
            <Campo
              type="text"
              ref={this.apellidosRef}
              maxLength="50"
              required
              placeholder="Apellidos"
            />
            <br />
            <Campo
              type="number"
              ref={this.telefonoRef}
              maxLength="10"
              required
              placeholder="Número a 10 Dígitos"
            />
            <br />
            <Campo
              type="email"
              ref={this.emailRef}
              maxLength="30"
              required
              placeholder="Correo electrónico"
            />
            <br />
            <Campo
              type="text"
              ref={this.asuntoRef}
              maxLength="30"
              required
              placeholder="Asunto"
            />
          </Formulario>
          <BtnSend onClick={e => this.enviarDatos(e)}>
          <Send
            enableBackground="new 0 0 64 64"
            id="Layer_1"
            version="1.1"
            viewBox="0 0 64 64"
            xmlSpace="preserve"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
          >
            <g>
              <rect height="4" width="21.959" y="21.125" />
              <rect height="4" width="16.016" x="5.943" y="29.454" />
              <rect height="4" width="10.01" x="11.949" y="38.875" />
            </g>
            <g id="Glyph_copy_2_1_">
              <path d="M63.508,19.53c-0.511-0.748-1.37-1.242-2.339-1.242H30.6c-0.949,0-1.788,0.477-2.303,1.199l17.643,14.711L63.508,19.53z" />
              <path d="M27.769,21.994v20.887c0,1.557,1.274,2.831,2.831,2.831h30.569c1.557,0,2.831-1.274,2.831-2.831V22.069L45.941,37.147   L27.769,21.994z" />
            </g>
          </Send>
          Enviar
          </BtnSend>
        </ContactDiv>
      </ContactContainer>
    );
  }
}

export default Contact;
