import React, { Component} from 'react';
import Slider from './Slider';
import styled from "styled-components";

const ServicesContainer = styled.div`
    width:100%;
    height: auto;
    display:block;
    @media (max-width:375px){
        display:block;
    }
    @media (max-width:414px){
        display:block;
    }
`;
const ServiceLeft = styled.div`
    width:100%;
    background-color:#C8102E;
    @media (max-width:375px){
        width:100%
    }
    @media (max-width:414px){
        width:100%
    }
`;
const ServiceRight = styled.div`
    width:100%;
    background-color:#000;
    @media (max-width:375px){
        width:100%;
    }
    @media (max-width:414px){
        width:100%;
    }
`;
const ServiceTitle = styled.h1`
    position:relative;
    margin:0;
    color:#C8102E;
    text-shadow: 0 0 4px #ffffff;
    font-size: 11vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    text-align:center;
    animation-duration: 3s;
    animation-name: air;
    animation-direction: alternate-reverse;
    animation-iteration-count: 2;
    animation-timing-function: ease-out;
    @media (max-width:375px){
        margin:0;
        font-size:13vw;
    }
    @media (max-width:414px){
        margin:0;
        font-size:13vw;
    }
    @keyframes air {
        from {
            left: 0;
        } 
        to {
            left: 50px;
        }
`;
const SubService = styled.h3`
    position:absolute;
    font-size: 7vw;
    font-family: 'Montserrat', sans-serif;
    font-weight:700;
    color:#3498DB;
    margin:0;
    margin-left:7%;
    z-index:100000;
    margin-top:-187px;
    @media (max-width:375px){
        margin-top:-10%;
        margin-left:42%;
    }
    @media (max-width:414px){
        margin-top:-10%;
        margin-left:42%;
    }
`;
const Breakdown = styled.p`
    width:100%;
    font-family: 'Ubuntu', sans-serif;
    font-size:2vw;
    font-weight:400;
    text-align: right;
    color:#3498DB;
    line-height:2.9vw;   
`;
class Products extends Component{
    render(){
        return(
            <React.Fragment>
                <ServicesContainer>
                    <ServiceLeft>
                        <ServiceTitle>PRODUCTOS</ServiceTitle>
                    </ServiceLeft>
                    <ServiceRight>
                        <Slider/>
                    </ServiceRight>
                </ServicesContainer>
            </React.Fragment>
        )
    }
}

export default Products;