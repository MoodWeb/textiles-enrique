import React, { Component} from 'react';
import ReactPlayer from 'react-player';
import './PreloaderStyles.css';

class Preloader extends Component {
    render(){
        return(
            <div className='player-wrapper'>
                <ReactPlayer
                    className='react-player'
                    url='https://youtu.be/3mIkWhWLls4'
                    width='100%'
                    height='100%'
                    playing
                />
            </div>
        )
    }
}

export default Preloader;