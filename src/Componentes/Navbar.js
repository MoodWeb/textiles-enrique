import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const NavContainer = styled.div`
    width:100%;
    margin:0;
    padding:0;
`;
const NavHeader = styled.nav`
    background-color:#ffffff;
    width:100%;
    height:auto;
    display:flex;
    justify-content:center;
    align-items:center;
    border-bottom:1px solid #C8102E;
`;
const ContainerLogo = styled.div`
    width:40%;
    margin-left:10%;
`;
const LogoTe = styled.img`
    position:relative;
    width:10vw;
    padding:20px 0 20px 0;
`;
const ContainerMenu = styled.div`
    width:50%;
`;
const MenuList = styled.ul`
    list-style-type:none;
    margin:0;
    padding:0 0 0 0;
    display:flex;
    justify-content:center;
    align-items:center;
    font-family: "Ubuntu", sans-serif;
    font-size:20px;
`;
const MenuOptions = styled.li`
    display:inline-block;
    justify-content:center;
    aling-content:center;
    margin-right:10px;
`;
const Options = styled.a`
    text-decoration:none;
    color:#000;
    cursor:pointer;
    &:hover{
        color:#C8102E;
    }
`;
class Navbar extends Component{
    render(){
        return(
            <NavContainer>
                <NavHeader>
                    <ContainerLogo>
                        <Link to="/"><LogoTe src="../img/LOGOTIPO_OFICIAL.png"/></Link>
                    </ContainerLogo>
                    <ContainerMenu>
                        <MenuList>
                            <Link to="/about"><MenuOptions><Options>Nosotros</Options></MenuOptions></Link>
                            <Link to="/products"><MenuOptions><Options>Productos</Options></MenuOptions></Link>
                            <Link to="/contact"><MenuOptions><Options>Contacto</Options></MenuOptions></Link>
                        </MenuList>
                    </ContainerMenu>
                </NavHeader>
            </NavContainer>
        )
    }
}

export default Navbar;