import React, { Component} from 'react';
import styled from "styled-components";

const FooterContainer = styled.div`
    width:100%;
    background-color:#ffffff;
    border-top:1px solid #C8102E;
    margin:0;
    padding:0;
`;
const RedesContainer = styled.div`
   margin:auto;
   width:50%;
   padding-top:20px;
`;
const RedesContact = styled.div`
    margin:auto;
    width:50%;
`;
const RedLink = styled.a`
    text-decoration:none;
    color:#5EC5E8;
`;
const ImgFooter = styled.img`
    display:block;
    margin-left:auto;
    margin-right:auto;
    width:15vw;
`;
const Facebook = styled.svg`
    display:block;
    width:4vw;
    margin:auto;
    fill:#000;
    &:hover{
        fill:#C8102E
    }
`;
const Derechos = styled.p`
    display:block;
    margin:0;
    padding-left:25%;
    padding-right:25%;
    padding-bottom:10px;
    color:#000;
    font-size:12px;
    font-family:'Titillium Web', sans-serif;
    font-weight:400;
    text-align:center;
    @media (max-width:375px){
        padding-left:10%;
        padding-right:10%;
    }
`;
class Footer extends Component{
    render(){
        return(
            <FooterContainer>
                <RedesContact>
                    <RedesContainer>
                        <RedLink href="https://www.facebook.com/TextilesEnriqueMx/" target="_blank" rel="noopener noreferrer">
                            <Facebook 
                                enableBackground="new 0 0 100 100" 
                                id="fb" 
                                version="1.1" 
                                viewBox="0 0 100 100" 
                                xmlSpace="preserve" 
                                xmlns="http://www.w3.org/2000/svg" 
                                xmlnsXlink="http://www.w3.org/1999/xlink">
                                <path d="M50,5C25.147,5,5,25.147,5,50c0,24.853,20.147,45,45,45c1.361,0,2.704-0.071,4.034-0.189  c0-10.463,0-20.925,0-31.387h-10.66V51.639c3.554,0,7.107,0,10.66,0c0-3.928,0-7.856,0-11.784c0-7.147,5.848-12.994,12.994-12.994h0  h12.179v8.52v2.702h-8.016c-2.571,0-4.656,2.085-4.656,4.656v8.901c1.98,0,3.96,0,5.94,0c2.129,0,4.257,0,6.386,0  c-0.542,3.928-1.084,7.856-1.625,11.784H66.536V91.85C83.204,85.258,95,69.012,95,50C95,25.147,74.853,5,50,5z">
                                </path>
                            </Facebook>
                        </RedLink>
                    </RedesContainer>
                    <ImgFooter src="../img/LOGO_HORIZONTAL.png"/>
                    <Derechos>© Copyright Textiles Enrique. Todos los derechos reservados.</Derechos>
                 </RedesContact>
            </FooterContainer>
        )
    }
}

export default Footer;