import React, { Component } from "react";
import Slider from "react-slick"; 
import styled from 'styled-components';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const SliderContainer = styled.div`
    width:90% !Important;
    margin:auto;
    padding:0;
`;
const SubTitle = styled.h2`
    margin:0;
    padding:0 0 15px 0;
    margin-top:10%;
    font-size:4vw;
    font-family: 'ubuntu', sans-serif;
    font-weight: 500;
    text-align:center;
    color:#ffffff;
`;
const ContainerPage = styled.div`
    width:100% !Important;
    height:auto;
    margin:0;
    padding:0;
`;
const ContentPage = styled.img`
    width:40vw;
    margin:auto;
    padding:5% 0 5% 0;
`;
const Info = styled.p`
    width:86%;
    font-size:2vw;
    font-family: 'Ubuntu', sans-serif;
    font-weight:400;
    color:#ffffff;
    margin:auto;
    padding:15px 0 15px 0;
    line-height:2.5vw;
    text-align:start;
`;
export default class AutoPlay extends Component {
    render() {
      const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 4000,
        cssEase: "linear"
      };

      return (
        <SliderContainer>
          <Slider {...settings}>
            <ContainerPage>
              <ContentPage src="../img/BOMBAY_BOLEADO.jpg"/>
              <Info>BOMBAY BOLEADO</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/BOMBAY.jpg"/>
              <Info>BOMBAY</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/BRUSH_coral.jpg"/>
              <Info>BRUSH CORAL</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/DUBLÍN_negro.jpg"/>
              <Info>DUBLÍN</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/FELPA_SPUN.jpg"/>
              <Info>FELPA SPUN</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/FRESH_TERRIER.jpg"/>
              <Info>FRESH TERRIER</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/HASHI_FUSIONADO.jpg"/>
              <Info>HASHI FUSIONADO</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/MESH.jpg"/>
              <Info>MESH</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/MICRO_PIQUÉ.jpg"/>
              <Info>MICRO PIQUÉ</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/MICROFIBRA_FUSIONADA.jpg"/>
              <Info>MICROFIBRA FUSIONADA</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/MICROFIBRA_FUSIONADA1.jpg"/>
              <Info>MICROFRIBRA FUSIONADA NEGRO</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/MICROFIBRA_FUSIONADO_vino.jpg"/>
              <Info>MICROFIBRA FUSIONADA VINO</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/MICROFIBRA_PIEL.jpg"/>
              <Info>MICROFIBRA PIEL</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/MILENIUM.jpg"/>
              <Info>MILENIUM</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/ORGANZA_CON_LENTEJUELA.jpg"/>
              <Info>ORGANZA CON LENTEJUELA</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/ORGANZA_LISTÓN.jpg"/>
              <Info>ORGANZA LISTÓN</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/SOCCER.jpg"/>
              <Info>SOCCER</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/SPORT_TOCK.jpg"/>
              <Info>SPORT TOCK</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/TAFETA.jpg"/>
              <Info>TAFETA</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/TAFETAN.jpg"/>
              <Info>TAFETAN</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/TIMBERLAND.jpg"/>
              <Info>TIMBERLAND</Info>
            </ContainerPage>
            <ContainerPage>
              <ContentPage src="../img/TUL_CON_LENTEJUELA.jpg"/>
              <Info>TUL CON LENTEJUELA</Info>
            </ContainerPage>
          </Slider>
        </SliderContainer>
      );
    }
  }
